<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Product;
use App\Http\Requests\ProductRequest;
use App\Repositories\Product\ProductInterface;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * @var ProductInterface
     */
    private $product;

    /**
     * ProductController constructor.
     * @param ProductInterface $product
     */
    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     * @param int $page
     * @return mixed
     */
    public function index($page = 1)
    {
        $limit = 10;
        $products = $this->product->paginate($limit, $page);
        return ProductResource::collection($products);
    }

    /**
     * Show the form for creating a new resource.
     * @param ProductRequest $request
     * @return mixed
     */
    public function create(ProductRequest $request)
    {
        $array = $request->all();
        $result  = Product::create($array);
        return new ProductResource($result);
    }

    /**
     * Display the specified resource.
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $product = $this->product->find($id);
        return new ProductResource($product);
    }

    /**
     * Update the specified resource in storage.
     * @param ProductRequest $request
     * @param $id
     * @return mixed
     */
    public function update(ProductRequest $request, $id)
    {
        $product = $this->product->find($id);
        $array = $request->all();
        $result = $product->update($array);
        return response()->success(['result' => $result]);
    }

    /**
     * @param $id
     * @return ProductResource
     * @throws \Exception
     */
    public function destroy($id)
    {
        $product = $this->product->find($id);
        $result = $product->delete();
        return response()->success(['result' => $result]);
    }
}
