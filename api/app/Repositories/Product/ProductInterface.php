<?php

namespace App\Repositories\Product;

/**
 * Interface ProductInterface
 * @package App\Repositories\Product
 */
interface ProductInterface
{
    /**
     * @param $limit
     * @param $page
     * @return mixed
     */
    public function paginate($limit, $page);


    /**
     * @param $id
     * @return mixed
     */
    public function find($id);
}