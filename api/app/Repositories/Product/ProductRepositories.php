<?php
namespace App\Repositories\Product;

use App\Http\Resources\ProductResource;
use App\Product;
use App\Repositories\Product\ProductInterface as ProductInterface;

/**
 * Class ProductRepositories
 * @package App\Repositories\Product
 */
class ProductRepositories implements ProductInterface
{
    /**
     * @var Product
     */
    public $propduct;

    /**
     * ProductRepositories constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->propduct = $product;
    }

    /**
     * @param $limit
     * @param $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|mixed
     */
    public function paginate($limit, $page)
    {
        return $this->propduct::orderBy('id', 'desc')
            ->paginate($limit, ['*'], 'page', $page);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed
     */
    public function find($id)
    {
        return $this->propduct::findOrFail($id);
    }
}