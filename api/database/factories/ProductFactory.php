<?php

use Faker\Generator as Faker;
use App\Category;

$factory->define(App\Product::class, function (Faker $faker) {
    $catMaxId = \App\Category::max('id');
    $catMinId = \App\Category::min('id');
    $img = ($faker->randomDigitNotNull) % 2 ? 1 : 2;
    return [
        'name' => $faker->firstNameFemale,
        'code' => $faker->swiftBicNumber,
        'img' => '/img/prod-'. $img .'.jpg',
        'price' => $faker->buildingNumber,
        'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'category_id' => function () use ($faker, $catMaxId, $catMinId) {
            return  Category::find($faker->numberBetween($min = $catMinId, $max = $catMaxId))->id;
        }
    ];
});
