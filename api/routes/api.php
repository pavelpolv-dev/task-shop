<?php

Route::get('products/{page?}', 'ProductController@index');
Route::get('product/{id}', 'ProductController@show');
Route::post('product', 'ProductController@create');
Route::patch('product/update/{id}', 'ProductController@update');
Route::delete('product/{id}', 'ProductController@destroy');
