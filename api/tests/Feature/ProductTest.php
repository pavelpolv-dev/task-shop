<?php

namespace Tests\Feature;

use App\Category;
use App\Product;
use Psy\Command\ListCommand\PropertyEnumerator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class ProductTest
 * @package Tests\Feature
 */
class ProductTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        factory(Category::class, 2)->create();
        factory(Product::class, 20)->create();
    }

    public function testSeeProductList()
    {
        $this->json('GET', '/api/products')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id', 'name', 'code', 'img', 'price', 'description', 'category_id',
                    ]
                ]
            ]);
    }

    public function testSeeProduct()
    {
        $id = Product::first()->id;
        $this->json('GET', '/api/product/' . $id)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id', 'name', 'code', 'img', 'price', 'description', 'category_id'
                ]
            ]);
    }

    public function testCreateProduct()
    {
        $id = Category::first()->id;
        $body = [
            'name' => 'Test Product Name',
            'code' => 'AAA1234',
            'img' => '/img/prod-1.jpg',
            'price' => 12,
            'description' => 'Description Product',
            'category_id' => $id
        ];
        $response = $this->withHeaders([
            ['Content-Type' => 'application/json'],
            ])->json('POST', '/api/product', $body);

        $response
            ->assertStatus(201)
            ->assertJson(['data' => $body])
            ->assertJsonStructure([
                'data' => [
                    'id', 'name', 'code', 'img', 'price', 'description', 'category_id'
                ]
            ]);
    }

    public function testUpdateProduct()
    {
        $idCat = Category::first()->id;
        $idProd = Product::first()->id;
        $body = [
            'name' => 'Test Product Name',
            'code' => 'AAA1234',
            'img' => '/img/prod-1.jpg',
            'price' => '12',
            'description' => 'Description Product',
            'category_id' => $idCat
        ];
        $response = $this->withHeaders([
            ['Content-Type' => 'application/json'],
        ])->json('PATCH', '/api/product/update/' . $idProd, $body);

        $response
            ->assertStatus(200)
            ->assertJson([
                'result' => true
            ]);
    }

    public function testDeleteProduct()
    {
        $id = Product::first()->id;
        $response = $this->withHeaders([
            ['Content-Type' => 'application/json'],
        ])->json('DELETE', '/api/product/' . $id);
        $response
            ->assertStatus(200)
            ->assertJson([
                'result' => true
            ]);
    }

    public function testBadRequest()
    {
        $body = [
            'name' => 'Test Product Name',
            'code' => 'AAA1234',
            'img' => '/img/prod-1.jpg',
            'description' => 'Description Product',
            'category_id' => 'abc'
        ];
        $response = $this->withHeaders([
            ['Content-Type' => 'application/json'],
        ])->json('POST', '/api/product', $body);
        $response
            ->assertStatus(422)
            ->assertJson([
                'error' => 'Invalid data',
            ]);
    }
}
