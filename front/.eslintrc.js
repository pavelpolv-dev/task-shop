module.exports = {
	'extends': 'airbnb',
	'plugins': [
		'react',
		'jsx-a11y',
	],
	'parser': 'babel-eslint',
	'rules': {
		'strict': 0,
		'no-tabs': 0,
		'react/jsx-filename-extension': [1, {'extensions': ['.js', '.jsx']}],
		'jsx-a11y/alt-text': [
			2, {
				'elements': ['img', 'object', 'area', 'input[type="image"]'],
				'img': ['Image'],
				'object': ['Object'],
				'area': ['Area'],
				'input[type="image"]': ['InputImage'],
			}],
		"react/jsx-indent": [2, 'tab'|2]
	},
	'env': {
		'browser': true,
		'node': true,
	},
};