import ActionType from '../constant/actionType';


export function appInit() {
  return (dispatch) => {
    dispatch({
      type: ActionType.APP_DENIED,
      payload: 'INITIALIZED',
    });
  };
}