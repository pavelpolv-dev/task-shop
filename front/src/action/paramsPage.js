import AppStatus from '../constant/appStatus';
import api from '../server';


export function productsListAction(num, fn) {
  return (dispatch) => {
    api.product.getProductsList(num)
      .then((response) => {
        dispatch({
          type: AppStatus.GET_PRODUCT_LIST,
          payload: response,
        });
        fn();
      });
  };
}

// TODO:: response.data надо исправить!!!
export function createProductAction(product, fn) {
  return (dispatch) => {
    api.product.createProduct(product)
      .then((response) => {
        dispatch({
          type: AppStatus.SET_PRODUCT_PARAMS,
          payload: response.data,
        });
        fn(response);
      });
  };
}

export function productsUpdateAction(id, data) {
  return (dispatch) => {
    api.product.editProduct(id, data)
      .then((response) => {
        dispatch({
          type: AppStatus.SET_ALERT_UP_PARAMS,
          payload: response.result,
        });
      });
  };
}


export function getProductAction(id) {
  return (dispatch) => {
    api.product.getProduct(id)
      .then((response) => {
        dispatch({
          type: AppStatus.GET_PRODUCT,
          payload: response.data,
        });
      });
  };
}


export function deleteProductAction(id) {
  return () => {
    api.product.deleteProduct(id)
      .then(response => (response));
  };
}

export function loaderActionTimeout(bool) {
  return (dispatch) => {
    setTimeout(() => {
      dispatch({
        type: AppStatus.INSTALL_LOADER,
        payload: bool,
      });
    }, 820);
  };
}

export function loaderAction(bool) {
  return (dispatch) => {
    dispatch({
      type: AppStatus.INSTALL_LOADER,
      payload: bool,
    });
  };
}

export function setProductAction(data) {
  return (dispatch) => {
    dispatch({
      type: AppStatus.SET_PRODUCT_PARAMS,
      payload: data,
    });
  };
}

export function controlAlertUpAction(boll) {
  return (dispatch) => {
    dispatch({
      type: AppStatus.SET_ALERT_UP_PARAMS,
      payload: boll,
    });
  };
}

export function numPageAction(num, fn) {
  return (dispatch) => {
    dispatch({
      type: AppStatus.SET_NUMBER_PAGE,
      payload: num,
    });
    fn();
  };
}