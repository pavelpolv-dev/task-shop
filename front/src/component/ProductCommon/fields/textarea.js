import React, { Component } from 'react';
import PropTypes from 'prop-types';
import helpers from '../../../component/helpers/validator';

export default class Textarea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value || '',
      valid: true,
    };
  }
  get valid() {
    return this.isValid();
  }
  get value() {
    return this.state.value;
  }
  changeInput(e) {
    const valueField = e.target.value;
    this.setState({
      value: valueField,
    }, () => (this.isValid()));
  }
  isValid() {
    let result = true;
    if (this.props.validatorName) {
      const func = helpers[this.props.validatorName];
      result = func(this.state.value);
    }
    this.setState({
      valid: result,
    });
    return result;
  }
  render() {
    return (
      <textarea
        className={`form-control ${this.state.valid ? '' : 'is-invalid'}`}
        onChange={(e) => this.changeInput(e)}
        rows={5}
        value={this.state.value}
      />
    );
  }
}

Textarea.defaultProps = {
  value: '',
  validatorName: PropTypes.string,
};
Textarea.propTypes = {
  value: PropTypes.string,
  validatorName: PropTypes.string,
};
