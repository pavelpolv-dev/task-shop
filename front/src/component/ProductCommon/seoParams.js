import React from 'react';

const SeoParams = () => (
  <div className="col-md-5 mt-2 mt-md-0">
    <div className="card">
      <div className="card-header">
        <h5>SEO-параметры</h5>
      </div>
      <div className="card-body">
        <div className="row form-group">
          <label className="col-sm-3 col-form-label">Title</label>
          <div className="col-sm-9">
            <input className="form-control" type="text" defaultValue="Купить вишню" />
          </div>
        </div>
        <div className="row form-group">
          <label className="col-sm-3 col-form-label">Description</label>
          <div className="col-sm-9">
            <textarea className="form-control" rows={3} defaultValue="null" />
          </div>
        </div>
        <div className="row form-group">
          <label className="col-sm-3 col-form-label">keywords</label>
          <div className="col-sm-9">
            <textarea className="form-control" rows={3} defaultValue="null" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default SeoParams;
