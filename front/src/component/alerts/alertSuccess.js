import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';

const AlertSuccess = props => (
  <CSSTransition
    in={props.show}
    timeout={1300}
    classNames="message"
    unmountOnExit
  >
    <div className="alert message alert-primary sticky-top" role="alert">
      {props.text}
    </div>
  </CSSTransition>
);

AlertSuccess.defaultProps = {
  text: '',
  show: true,
};

AlertSuccess.propTypes = {
  text: PropTypes.string,
  show: PropTypes.bool,
};
export default AlertSuccess;
