import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import PropTypes from 'prop-types';

const AnimationWrapper = props => (
  <ReactCSSTransitionGroup
    transitionAppear={true}
    transitionAppearTimeout={400}
    transitionEnterTimeout={800}
    transitionLeaveTimeout={800}
    transitionName="animation"
    component="div"
    className="sub-component"
  >
    { props.children }
  </ReactCSSTransitionGroup>
);

AnimationWrapper.defaultProps = {
  children: null,
};

AnimationWrapper.propTypes = {
  children: PropTypes.node,
};


export default AnimationWrapper;
