import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

const PageShell = Page => (
  props => (
    <ReactCSSTransitionGroup
      transitionAppear={true}
      transitionAppearTimeout={100}
      transitionEnterTimeout={100}
      transitionLeaveTimeout={100}
      transitionName="fade"
      component="div"
      className="col-12"
    >
      <div className="component-out">
        <Page {...props} />
      </div>
    </ReactCSSTransitionGroup>
  )
);

export default PageShell;
