import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Page from '../pages/routers';
import Loader from '../component/loader/full-loader';
import { appInit } from '../action/appInit';
import PropTypes from 'prop-types';

const pageStatus = {
  UNDEFINED: <Loader />,
  INITIALIZED: <Page />,
};

@connect(state => ({
  status: state.appStatus.pageName,
}), { appInit })

export default class App extends Component {
  state = {
    page: 'UNDEFINED',
  };
  static getDerivedStateFromProps(props, state) {
    if (props.status !== undefined && props.status !== state.pageName) {
      return { page: props.status };
    }
    return null;
  }
  componentDidMount() {
    this.props.appInit();
  }
  render() {
    return (
      <BrowserRouter>
        {pageStatus[this.state.page]}
      </BrowserRouter>
    );
  }
}

App.defaultProps = {
  status: 'UNDEFINED',

};

App.propTypes = {
  status: PropTypes.string,
  appInit: PropTypes.func,
};
