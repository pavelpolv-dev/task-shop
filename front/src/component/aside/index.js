import React from 'react';

const Aside = () => (
  <aside className="bg-dark-gray">
    <div className="avatar row no-gutters d-flex py-4">
      <div className="col-auto">
        <img className="rounded-circle ml-2" src="/img/avatar.jpg" alt="avatar"/>
      </div>
      <div className="col ml-3 align-self-center">
        <strong>Виктор Теплов</strong><br />
        <small>Super Admin</small>
      </div>
    </div>
    <ul className="nav flex-column">
      <li className="nav-item">
        <a className="nav-link" href="/">Категории</a>
      </li>
      <li className="nav-item">
        <a className="nav-link active" href="/">Товары</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/">Заказы</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/">Склад</a>
      </li>
    </ul>
  </aside>
);

export default Aside;
