import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import Navbar from '../navbar';
import Aside from '../aside';
import AnimationWrapper from '../animation/animation_wrapper';

const Global = props => (
  <Fragment>
    <Navbar />
    <div className="content-wrapper">
      <Aside />
      <div className="content-block">
        <div className="sub-wrapper">
            {props.children}
        </div>
      </div>
    </div>
  </Fragment>
);

Global.defaultProps = {
  children: null,
};

Global.propTypes = {
  children: PropTypes.node,
};

export default Global;
