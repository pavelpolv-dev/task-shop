const validator = {
  stringEmpty: str => (str.length > 0),
  numberPositive: n => (!isNaN(Number(n)) && n > 0),
};

export default validator;
