import React from 'react';

const FullLoader = () => (
  <div id="LOADER" className="component">
    <div className="loader d-flex align-items-center justify-content-center">
      <img src="/img/svg/loader.svg" alt="loader" />
    </div>
  </div>
);

export default FullLoader;
