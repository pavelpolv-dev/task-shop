import React from 'react';

const Navbar = () => (
	<header className="bg-light fixed-top">
		<div className="row no-gutters justify-content-between">
			<div className="col-auto">
				<div className="logo-line">
					<div className="logo">
						<a className="d-flex" href="/">
							<img className="align-self-center" src="/img/logo.png" alt="logo" />
							<span className="align-self-center"> TaskShop </span>
						</a>
					</div>
				</div>
			</div>
			<div className="col-auto">
				<nav className="navbar navbar-expand-lg">
					<div className="collapse navbar-collapse">
						<ul className="navbar-nav">
							<li className="nav-item notif">
								<a className="nav-link" href="/">
									<img className="pl-2" src="/img/svg/bell.svg" height="20px" alt="bell" />
								</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="/#">
									<img className="pl-2" src="/img/svg/mail.svg" height="20px" alt="mail" />
								</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="/#">
									<img className="px-2" src="/img/svg/setting.svg" height="20px" alt="setting" />
								</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</header>
);

export default Navbar;

