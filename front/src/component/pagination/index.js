import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Pagination extends Component {
  state = {
    page: this.props.page || 1,
    offset: this.props.offset,
    lastPage: this.props.lastPage,
    limit: 10,
  }
  /**
   * @param num
   */
  clickLink(num) {
    this.setState({
      page: num,
    });
    this.props.clickPage(num);
  }

  /**
   * @returns {Array}
   */
  pageGet() {
    const lastPage = this.state.lastPage;
    let curr = this.state.page;
    let start = curr - 1;
    let end = curr + 1;
    let array = [];
    if (curr === 1) {
      start = 1;
      end = lastPage === 2 ? 2 : 3;
    }
    if (curr === lastPage) {
      start = lastPage === 2 ? 1 : lastPage - 2;
      end = lastPage;
    }
    for (let i = start; i < end + 1; i++) {
      array.push(i);
    }
    return array;
  }

  render() {
    const lastPage = this.state.lastPage;
    const pages = this.pageGet();
    const list = pages.map(item => {
      return (
        <li key={item} className={`${this.state.page === item ? 'disabled' : ''} page-item`}>
          <a className="page-link" onClick={() => this.clickLink(item)}>
            { item }
          </a>
        </li>
      );
    });
    return (
      <div className="float-right">
        <nav>
          <ul className="pagination justify-content-end mb-0">
            <li className="page-item"><a className="page-link" onClick={() => this.clickLink(1)}>«</a></li>
            {list}
            <li className="page-item"><a className="page-link" onClick={() => this.clickLink(lastPage)}>»</a></li>
          </ul>
        </nav>
      </div>
    );
  }
}

Pagination.propsTypes = {
  page: PropTypes.number.isRequired,
  clickPage: PropTypes.func.isRequired,
  offset: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
};
