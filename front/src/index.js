import React from 'react';
import ReactDOM from 'react-dom';
import Root from './root';

import store from './store';


window.store = store;


ReactDOM.render(<Root store={store} />, document.getElementById('root'));

if (module.hot) {
  module.hot.accept('./root', () => render(Root));
}
