import React, { Component } from 'react';
import PropTypes from 'prop-types';

const Breadcrumb = props => (
  <nav className="col-auto">
    <ol className="breadcrumb">
      <li className="breadcrumb-item">
        <span className="fakeLink" onClick={() => (props.clickToList())}>Главная</span>
      </li>
      <li className="breadcrumb-item">
        <span className="fakeLink" onClick={() => (props.clickToList())}>Список продуктов</span>
      </li>
      <li className="breadcrumb-item active">Редактировать</li>
    </ol>
  </nav>
);

Breadcrumb.propTypes = {
  clickToList: PropTypes.func.isRequired,
};

export default Breadcrumb;
