import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';


import Breadcrumb from './breadcrumb';
import SeoParams from '../../../component/ProductCommon/seoParams';
import TextInput from '../../../component/ProductCommon/fields/textInput';
import Textarea from '../../../component/ProductCommon/fields/textarea';
export default class CreateForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      img: '/img/no_image.jpg',
    };
    this.nameField = React.createRef();
    this.codeField = React.createRef();
    this.priceField = React.createRef();
    this.textField = React.createRef();
  }
  refNames() {
    return ['nameField', 'codeField', 'priceField', 'textField'];
  }
  fileUploaded(e) {
    const reader = new FileReader();
    const file = e.target.files[0];
    reader.onloadend = () => {
      this.setState({
        file: file,
        img: reader.result,
      });
    };
    reader.readAsDataURL(file);
  }

  clickSave() {
    const valid = this.isValid();
    if (valid) {
      const product = this.buildProduct();
      this.props.createProduct(product);
    }
  }
  isValid() {
    const array = this.refNames();
    return array.reduce((res, cur) => (
      this[cur].current.valid && res
    ), true);
  }
  buildProduct() {
    return {
      name: this.nameField.current.value,
      code: this.codeField.current.value,
      img: '/img/prod-1.jpg',
      price: this.priceField.current.value,
      description: this.textField.current.value,
      category_id: 1,
    };
  }

  render() {
    const { img } = this.state;
    return (
      <Fragment>
        <div className="component">
          <div className="container-fluid">
            <div className="row justify-content-between">
              <h3 className="col-auto">Редактировать продукт</h3>
              <Breadcrumb clickToList={this.props.clickToList} />
            </div>
          </div>
          <div className="container-fluid" />
          <div className="row">
            <div className="col-md-7">
              <div className="card">
                <div className="card-header">
                  <h5>Основные</h5>
                </div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-auto">
                      <div className="prevImg" style={{ backgroundImage: 'url(' + img + ')' }} />
                    </div>
                    <div className="col">
                      <div className="row form-group">
                        <label className="col-sm-3 col-form-label">Название*</label>
                        <div className="col-sm-9">
                          <TextInput validatorName="stringEmpty" ref={this.nameField} />
                        </div>
                      </div>
                      <div className="row form-group">
                        <label className="col-sm-3 col-form-label">Артикул*</label>
                        <div className="col-sm-9">
                          <TextInput validatorName="stringEmpty" ref={this.codeField} />
                        </div>
                      </div>
                      <div className="row form-group">
                        <label className="col-sm-3 col-form-label">Изображение</label>
                        <div className="col-sm-9">
                          <div className="custom-file">
                            <input className="custom-file-input" type="file" onChange={event => this.fileUploaded(event)} />
                            <label className="custom-file-label">{this.state.file ? this.state.file.name : img}</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row form-group">
                    <label className="col-sm-2 col-form-label">Цена*</label>
                    <div className="col-sm-10">
                      <TextInput validatorName="numberPositive" ref={this.priceField} />
                    </div>
                  </div>
                  <div className="row form-group">
                    <label className="col-sm-2 col-form-label">Описание*</label>
                    <div className="col-sm-10">
                      <Textarea validatorName="stringEmpty" ref={this.textField} />
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-sm-2">Публикация</div>
                    <div className="col-sm-10">
                      <div className="form-check">
                        <input className="form-check-input" type="checkbox" defaultChecked />
                      </div>
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-12 text-right">
                      <button className="btn btn-outline-success" onClick={() => (this.props.clickToList())}>К списку</button>
                      <button
                        className="btn btn-outline-primary ml-1"
                        onClick={() => (this.clickSave())}
                      >
                        Сохранить
                      </button>
                    </div>
                  </div>
                  <small>Поля со знаком * имеют валидацию.</small>
                </div>
              </div>
            </div>
            <SeoParams />
          </div>
        </div>
      </Fragment>
    );
  }
}

CreateForm.defaultProps = {
  clickToList: () => {
    this.props.history.push('/product/');
  },
  createProduct: () => {},
};

CreateForm.propTypes = {
  clickToList: PropTypes.func,
  createProduct: PropTypes.func,
};
