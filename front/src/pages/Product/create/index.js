import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { createProductAction, loaderActionTimeout, loaderAction } from '../../../action/paramsPage';

import Loader from '../../../component/loader/full-loader';
import Global from '../../../component/global';
import CreateForm from './createForm'
import AnimationWrapper from '../../../component/animation/animation_wrapper';

@withRouter

@connect(state => ({
  loader: state.pageParam.loader,
}), {
  createProductAction, loaderActionTimeout, loaderAction
})

export default class Create extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: this.props.loader,
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.loader !== state.loader) {
      return {
        ...state,
        ...{
          loader: props.loader,
        },
      };
    }
    return null;
  }
  componentDidMount() {
    this.props.loaderActionTimeout(false);
  }
  createProduct() {
    return (product) => {
      this.props.createProductAction(product, (response) => {
        const id = response.data.id;
        this.props.loaderAction(true);
        this.props.history.push('/product/edit/' + id);
      });
    };
  }
  clickToList() {
    return () => {
      this.props.loaderAction(true);
      this.props.history.push('/product/');
    };
  }
  render() {
    return (
      <Global>
        <AnimationWrapper>
          { this.state.loader ? <Loader key={1} /> :
          <CreateForm
            createProduct={this.createProduct()}
            clickToList={this.clickToList()}
          />
          }
        </AnimationWrapper>
      </Global>
    );
  }
}

Create.defaultProps = {
  loaderActionTimeout: () => {},
  loaderAction: () => {},
  createProductAction: () => {},
};

Create.propTypes = {
  loaderActionTimeout: PropTypes.func,
  loaderAction: PropTypes.func,
  createProductAction: PropTypes.func,
};
