import React, { Component } from 'react';
import { connect } from 'react-redux';

import EditForm from './editForm';
import Loader from '../../../component/loader/full-loader';
import Global from '../../../component/global';
import AnimationWrapper from '../../../component/animation/animation_wrapper';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';

import {
  getProductAction,
  loaderAction,
  productsUpdateAction,
  loaderActionTimeout,
  setProductAction,
  controlAlertUpAction,
} from '../../../action/paramsPage';

@withRouter

@connect(state => ({
  product: state.pageParam.product,
  loader: state.pageParam.loader,
  alert: state.pageParam.alert,
}), {
  getProductAction,
  loaderAction,
  loaderActionTimeout,
  productsUpdateAction,
  setProductAction,
  controlAlertUpAction,
})

export default class EditProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: this.props.loader,
      product: this.props.product,
      alertUp: false,
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (state.product !== props.product && props.product.id) {
      return {
        product: props.product,
      };
    }
    if (state.alertUp !== props.alert.update) {
      return {
        ...state,
        ...{
          alertUp: props.alert.update,
        },
      };
    }
    if (props.loader !== state.loader) {
      return {
        ...state,
        ...{
          loader: props.loader,
        },
      };
    }
    return null;
  }
  componentDidMount() {
    const id = this.props.match.params.id;
    if (this.props.product.id !== Number(id)) {
      this.props.getProductAction(id);
    } else {
      this.props.loaderActionTimeout(false);
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(prevState.product) !== JSON.stringify(this.state.product)) {
      this.props.loaderActionTimeout(false);
    }
  }
  clickToList() {
    return () => {
      this.props.loaderAction(true);
      this.props.history.push('/product/');
    };
  }
  clickSave() {
    return (id, data) => {
      this.props.productsUpdateAction(id, data);
      this.props.setProductAction(data);
    };
  }
  removeAlert() {
    return () => (
      this.props.controlAlertUpAction(false)
    );
  }
  render() {
    return (
      <Global>
        <AnimationWrapper>
          { this.state.loader ? <Loader key={1} /> :
          <EditForm
            removeAlert={this.removeAlert()}
            alertUp={this.state.alertUp}
            clickSave={this.clickSave()}
            clickToList={this.clickToList()}
            product={this.state.product}
          />
          }
        </AnimationWrapper>
      </Global>
    );
  }
}

EditProduct.defaultProps = {
  getProductAction: () => {},
  productsUpdateAction: () => {},
  loaderAction: () => {},
  setProductAction: () => {},
  loaderActionTimeout: () => {},
  controlAlertUpAction: () => {},
  history: {},
  product: {},
  loader: true,
  alertUp: false,
};

EditProduct.propTypes = {
  getProductAction: PropTypes.func,
  controlAlertUpAction: PropTypes.func,
  productsUpdateAction: PropTypes.func,
  setProductAction: PropTypes.func,
  loaderActionTimeout: PropTypes.func,
  loaderAction: PropTypes.func,
  product: PropTypes.object,
  history: PropTypes.object,
  loader: PropTypes.bool,
  alertUp: PropTypes.bool,
};
