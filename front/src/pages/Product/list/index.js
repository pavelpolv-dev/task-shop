import React, { Component } from 'react';

import Loader from '../../../component/loader/full-loader';
import Global from '../../../component/global';
import AnimationWrapper from '../../../component/animation/animation_wrapper';
import ListingProducts from './listingProducts';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';


import {
  productsListAction,
  deleteProductAction,
  loaderActionTimeout,
  loaderAction,
  numPageAction,
} from '../../../action/paramsPage';

@withRouter

@connect(
  state => ({
    products: state.pageParam.products,
    loader: state.pageParam.loader,
    page: state.pageParam.page,
  }),
  {
    productsListAction, deleteProductAction, loaderActionTimeout, loaderAction, numPageAction,
  },
)

export default class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      loader: this.props.loader,
      page: this.props.page,
      lastPage: 1,
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.products.data !== state.products) {
      const newState = {
        products: props.products.data,
        lastPage: props.products.meta.last_page,
      };
      return { ...state, ...newState };
    }
    if (props.page !== state.page || props.loader !== state.loader) {
      return {
        ...state,
        ...{ page: props.page, loader: props.loader },
      };
    }
    return null;
  }
  componentDidMount() {
    this.getProducts();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.products === this.state.products) {
      this.props.loaderActionTimeout(false);
    }
    if (prevState.page !== this.state.page) {
      this.getProducts();
    }
  }

  setPageName() {
    return (id, action) => {
      this.props.history.push('/product/' + action + '/' + id);
      this.props.loaderAction(true);
    };
  }

  toCreatePage() {
    return () => {
      this.props.history.push('/product/create');
      this.props.loaderAction(true);
    };
  }

  getProducts() {
    this.props.productsListAction(this.state.page, () => {
      this.props.loaderActionTimeout(false);
    });
  }
  clickPage() {
    return num => (
      this.props.numPageAction(num, () => {
        this.props.loaderAction(true);
      })
    );
  }
  deleteItem() {
    return (id) => {
      this.props.deleteProductAction(id);
    };
  }
  render() {
    return (
      <Global>
        <AnimationWrapper>
          { this.state.loader ? <Loader key={1} /> :
          <ListingProducts
            {...this.props}
            clickPage={this.clickPage()}
            setPageName={this.setPageName()}
            deleteItem={this.deleteItem()}
            toCreatePage={this.toCreatePage()}
            lastPage={this.state.lastPage}
            list={this.state.products}
            page={this.state.page}
            key={2}
          /> }
        </AnimationWrapper>
      </Global>
    );
  }
}

List.defaultProps = {
  productsListAction: null,
  deleteProductAction: null,
  loaderActionTimeout: () => {},
  loaderAction: () => {},
  numPageAction: () => {},
  loader: true,
  history: {},
};

List.propTypes = {
  productsListAction: PropTypes.func,
  deleteProductAction: PropTypes.func,
  loaderActionTimeout: PropTypes.func,
  loaderAction: PropTypes.func,
  numPageAction: PropTypes.func,
  history: PropTypes.object,
  loader: PropTypes.bool,
  page: PropTypes.string,
};
