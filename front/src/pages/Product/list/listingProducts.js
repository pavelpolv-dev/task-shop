import React, { Component } from 'react';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import Pagination from '../../../component/pagination';

const listProduct = props => (
  props.products.map((item, index) => {
    const num = (props.page - 1) * 10 + (index + 1);
    return (
      <CSSTransition
        key={item.id}
        timeout={420}
        classNames="out">
        <tr className="line">
          <td scope="row">{ num }</td>
          <td>{item.code}</td>
          <td>
            <img src={item.img} height="40px"/>
          </td>
          <td><span className="fakeLink" onClick={() => (props.setPageName(item.id, 'edit'))}>{item.name}</span></td>
          <td>$ {item.price}</td>
          <td>
            <img src="/img/svg/plus.svg" alt="public"/>
          </td>
          <td>23-04-2018</td>
          <td>
            <div className="btn-group btn-group-sm" role="group">
              <button className="btn btn-outline-success" onClick={() => (props.setPageName(item.id, 'show'))}>
                <img src="/img/svg/see.svg" height="16px" alt="see"/>
              </button>
              <button className="btn btn-outline-primary" onClick={() => (props.setPageName(item.id, 'edit'))}>
                <img src="/img/svg/edit.svg" height="16px" alt="edit"/>
              </button>
              <button className="btn btn-outline-danger" onClick={() => (props.deleteItem(item.id))}>
                <img src="/img/svg/delete.svg" height="16px" alt="delete"/>
              </button>
            </div>
          </td>
        </tr>
      </CSSTransition>
    );
  })
);

export default class List extends Component {
  state = {
    products: this.props.list,
  }

  deleteItem() {
    return (id) => {
      const products = this.searchProduct(id);
      this.setState({ products: products });
      this.props.deleteItem(id);
    };
  }
  searchProduct(id) {
    const prod = this.state.products.filter(product => (product.id !== id));
    return prod;
  }
  render() {
    return (
      <div className="component">
        <div className="container-fluid">
          <div className="row justify-content-between">
            <h3 className="col-auto">Список продуктов</h3>
            <nav className="col-auto">
              <ol className="breadcrumb">
                <li className="breadcrumb-item"><a href="#">Главная</a></li>
                <li className="breadcrumb-item active">Список продуктов</li>
              </ol>
            </nav>
          </div>
        </div>
        <div className="container-fluid"/>
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-header">
                <div className="d-flex justify-content-between">
                  <h5>Продукты</h5>
                  <button
                    className="btn btn-outline-primary"
                    onClick={() => { this.props.toCreatePage(); }} >
                    Создать
                  </button>
                </div>

              </div>
              <div className="card-body">
                <table className="table">
                  <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Арт</th>
                    <th scope="col">Вид</th>
                    <th scope="col">Название</th>
                    <th scope="col">Цена</th>
                    <th scope="col">Публикация</th>
                    <th scope="col">Дата</th>
                    <th scope="col">Действия</th>
                  </tr>
                  </thead>
                  <TransitionGroup className="list-item" component="tbody">
                    { listProduct({
                      products: this.state.products,
                      page: this.props.page,
                      setPageName: this.props.setPageName,
                      deleteItem: this.deleteItem(),
                    }) }
                  </TransitionGroup>
                </table>
                <div className="d-flex align-items-center justify-content-center">
                  <Pagination {...this.props} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
