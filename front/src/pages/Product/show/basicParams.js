import React from 'react';
import PropTypes from 'prop-types';


const BasicParams = (props) => {
  const {
    code, description, img, name, price,
  } = props.product;
  const bg = img ? img : '/img/no_image.jpg';
  return (
    <div className="col-md-8">
      <div className="card">
        <div className="card-header">
          <h5>Основные</h5>
        </div>
        <div className="card-body">
          <div className="row">
            <div className="col-3">
              <div className="prevImg" style={{ backgroundImage: 'url(' + bg + ')' }} />
            </div>
            <div className="col-9">
              <h3>{ name }</h3>
              <span><strong>Арт:</strong>&nbsp;{ code }</span>
              <p><strong>Цена:</strong>&nbsp;$ { price }</p>
              <p>{ description }</p>
              <div className="float-right">
                <button onClick={() => { props.clickToList(); }} className="btn btn-outline-success">К списку</button>
                <button onClick={() => { props.clickToEdit(); }} className="btn btn-outline-primary ml-1">Редактировать</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

BasicParams.defaultProps = {
  product : {},
};

BasicParams.propTypes = {
  product: PropTypes.object.isRequired,
  clickToList: PropTypes.func.isRequired,
  clickToEdit: PropTypes.func.isRequired,
};

export default BasicParams;
