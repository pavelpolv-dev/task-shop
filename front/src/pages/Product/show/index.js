import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';

import View from './view';
import Loader from '../../../component/loader/full-loader';
import Global from '../../../component/global';
import AnimationWrapper from '../../../component/animation/animation_wrapper';
import {
  getProductAction,
  loaderActionTimeout,
  loaderAction,
} from '../../../action/paramsPage';


@withRouter
@connect(
  state => ({
    product: state.pageParam.product,
    loader: state.pageParam.loader,
  }),
  {
    getProductAction,
    loaderActionTimeout,
    loaderAction,
  },
)
export default class Show extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      product: this.props.product,
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (state.product !== props.product && props.product.id) {
      return {
        product: props.product,
      };
    }
    if (props.loader !== state.loader) {
      return {
        ...state,
        ...{
          loader: props.loader,
        },
      };
    }
    return null;
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    if (this.props.product.id !== Number(id)) {
      this.props.getProductAction(id);
    } else {
      this.props.loaderActionTimeout(false);
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(prevState.product) !== JSON.stringify(this.state.product)) {
      this.props.loaderActionTimeout(false);
    }
  }
  clickToList() {
    return () => {
      this.props.loaderAction(true);
      this.props.history.push('/product/');
    };
  }
  clickToEdit() {
    return () => {
      const id = this.props.match.params.id;
      this.props.loaderAction(true);
      this.props.history.push('/product/edit/' + id);
    };
  }
  render() {
    return (
      <Global>
        <AnimationWrapper>
          { this.state.loader ? <Loader key={3} /> :
          <View
            product={this.state.product}
            clickToList={this.clickToList()}
            clickToEdit={this.clickToEdit()}
          />
          }
        </AnimationWrapper>
      </Global>
    );
  }
}


Show.defaultProps = {
  getProductAction: () => {},
  loaderActionTimeout: () => {},
  loaderAction: () => {},
  history: {},
  product: {},
  loader: true,
};

Show.propTypes = {
  getProductAction: PropTypes.func,
  loaderActionTimeout: PropTypes.func,
  loaderAction: PropTypes.func,
  history: PropTypes.object,
  product: PropTypes.object,
  loader: PropTypes.bool,
};
