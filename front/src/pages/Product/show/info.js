import React from 'react';

const Info = () => (
  <div className="col-md-4 mt-2 mt-md-0">
    <div className="card">
      <div className="card-header">
        <h5>Информация</h5>
      </div>
      <div className="card-body">
        <table className="table">
          <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Параметр</th>
            <th scope="col">Значение</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>1</td>
            <td>Поступление</td>
            <td>23-05-18</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Остаток</td>
            <td>18</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Рейтинг</td>
            <td>14,2</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Показы</td>
            <td>1821</td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
);

export default Info;
