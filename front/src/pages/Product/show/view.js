import React from 'react';
import PropTypes from 'prop-types';
import BasicParams from './basicParams';
import Info from './info';

const View = (props) => {
  return (
    <div className="component">
      <div className="container-fluid">
        <div className="row justify-content-between">
          <h3 className="col-auto">{ props.product.name}</h3>
          <nav className="col-auto">
            <ol className="breadcrumb">
              <li className="breadcrumb-item"><span className="fakeLink" onClick={() => { props.clickToList(); }}>Главная</span></li>
              <li className="breadcrumb-item"><span className="fakeLink" onClick={() => { props.clickToList(); }}>Список продуктов</span></li>
              <li className="breadcrumb-item active">{ props.product.name }</li>
            </ol>
          </nav>
        </div>
      </div>
      <div className="container-fluid" />
      <div className="row">
        <BasicParams {...props} />
        <Info date={props.product.date} />
      </div>
    </div>
  );
};

View.propTypes = {
  product: PropTypes.object.isRequired,
  clickToList: PropTypes.func.isRequired,
};

export default View;
