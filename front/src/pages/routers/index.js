import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import List from '../Product/list';
import EditProduct from '../Product/edit/editProduct';
import Show from '../Product/show';
import Create from '../Product/create';
import PageShell from '../../component/animation/page_shell';


const Page = () => (
  <div className="application">
    <Switch>
      <Route exact path="/product" component={PageShell(List)} />
      <Route exact path="/product/edit/:id?" component={PageShell(EditProduct)} />
      <Route exact path="/product/show/:id?" component={PageShell(Show)} />
      <Route exact path="/product/create" component={PageShell(Create)} />
      <Route path="/" render={() => (<Redirect to={'/product'} />)} />
    </Switch>
  </div>
);

export default Page;
