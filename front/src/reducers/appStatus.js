import AppStatus from '../constant/appStatus';
import ActionType from '../constant/actionType';
import createReducer from '../utils/reducerCreator';

const defaultState = {
  pageName: null,
};
const actionHandlers = {
  [ActionType.APP_DENIED]: (state, payload) => ({ ...state, pageName: payload }),


};

export default createReducer(actionHandlers, defaultState);