import pageParam from './pageParam';
import appStatus from './appStatus';

export default {
  pageParam,
  appStatus,
};
