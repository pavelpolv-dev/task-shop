import createReducer from '../utils/reducerCreator';
import ActionType from '../constant/actionType';
import AppStatus from '../constant/appStatus';

const defaultState = {
  alert: {
    update: false,
  },
  result: false,
  loader: true,
  id: null,
  products: {
    meta: {
      current_page: 0,
    },
  },
  product: {},
  page: 1,
};
const actionHandlers = {
  [AppStatus.GET_PRODUCT_LIST]: (state, payload) => ({ ...state, products: payload }),
  [AppStatus.GET_PRODUCT]: (state, payload) => ({ ...state, product: payload }),
  [AppStatus.SET_PRODUCT_PARAMS]: (state, payload) => ({ ...state, product: payload }),
  [AppStatus.INSTALL_LOADER]: (state, payload) => ({ ...state, loader: payload }),
  [AppStatus.SET_NUMBER_PAGE]: (state, payload) => ({ ...state, page: payload }),
  [AppStatus.SET_ALERT_UP_PARAMS]: (state, payload) => ({ ...state, alert: { update: payload } }),
};

export default createReducer(actionHandlers, defaultState);
