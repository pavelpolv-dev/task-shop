
import React from 'react';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';

import App from './component/app';

const Root = props => (
  <Provider store={props.store}>
    <App />
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object,
};

export default Root;
