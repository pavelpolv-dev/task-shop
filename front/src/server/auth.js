import {get, post} from './request-manager/http'


function token() {
  return new Promise((resolve, reject) => {
    post('api/auth/app')
      .then(response => {
        resolve(response.data);
      })
      .catch(e => {
          console.log(e)
        reject(e);
      });
  });
}

function authUser(email, pass) {
  return new Promise((resolve, reject) => {
    post('api/auth/user', { email: email, password: pass })
      .then(response => {
        resolve(response);
      })
      .catch(e => {
        console.log(e)
        reject(e);
      });
  });
}


//Узнаем авторизован ли пользователь и жива ли его сессия
function check() {
  return new Promise((resolve, reject) => {
    get('/rest/auth/check')
      .then(response => {
        resolve(response.data.auth);
      })
      .catch(e => {
        reject(e);
      });
  });
}

function login(login, password) {
  return new Promise((resolve, reject) => {
    post('/rest/auth/login', {login: login, password: password})
      .then(response => {
        if (!response.error) {
          resolve();
        } else {
          reject(response.message);
        }
      })
      .catch(reject);
  });
}

export default {
    login,
    token,
    authUser,
    check
}