import { get, post, patch, del } from './request-manager/http';

const getProductsList = page => (
  new Promise((resolve, reject) => (
    get(`products/${page}`)
      .then((response) => {
        resolve(response.data);
      }).catch((e) => {
        console.log(e);
        reject(e);
      })
  ))
);

const getProduct = id => (
  new Promise((resolve, reject) => (
    get(`product/${id}`)
      .then((response) => {
        resolve(response.data);
      }).catch((e) => {
        console.log(e);
        reject(e);
      })
  ))
);

const createProduct = data => (
  new Promise((resolve, reject) => (
    post('product', data)
      .then((response) => {
        resolve(response.data);
      }).catch((e) => {
        console.log(e);
        reject(e);
      })
  ))
);


const editProduct = (id, data) => (
  new Promise((resolve, reject) => (
    patch(`product/update/${id}`, data)
      .then((response) => {
        resolve(response.data);
      }).catch((e) => {
        console.log(e);
        reject(e);
      })
  ))
);

const deleteProduct = id => (
  new Promise((resolve, reject) => (
    del(`product/${id}`)
      .then((response) => {
        resolve(response.data);
      }).catch((e) => {
        console.log(e);
        reject(e);
      })
  ))
);


export default {
  getProductsList,
  getProduct,
  createProduct,
  editProduct,
  deleteProduct,
};
