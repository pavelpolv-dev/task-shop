import axios from 'axios';

const connection = axios.create({
  baseURL: 'http://127.0.0.1:8000/api/',
});

function PreProcessor(axiosPromise) {
  return new Promise((resolve, reject) => {
    axiosPromise.then((response) => {
      resolve(response);
    }).catch((error) => {
      console.log('axios', error);
      // window.location.href = '/error';
      reject(error);
    });
  });
}

function get(url, config) {
  return PreProcessor(connection.get(url, config));
}

function post(url, data, config) {
  return PreProcessor(connection.post(url, data, config));
}

function put(url, data, config) {
  return PreProcessor(connection.put(url, data, config));
}

function patch(url, data, config) {
  return PreProcessor(connection.patch(url, data, config));
}

function del(url) {
  return PreProcessor(connection.delete(url));
}

export { get, post, put, patch, del };
