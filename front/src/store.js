import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

function compileReducers() {
  let result = {};
  for (let name in reducers) {
    let reducer = reducers[name];
    result[name] = reducer;
  }
  return combineReducers(result);
}

function compileMiddlewares() {
  return applyMiddleware(thunk);
}

const store = createStore(compileReducers(), {} , compileMiddlewares());
export default store;
