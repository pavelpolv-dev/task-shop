"use strict";
var pages = [
    {
        template: './src/jade/index.pug',
        filename: 'index.html'
    },
    {
        template: './src/jade/loader.pug',
        filename: 'loader.html'
    },
    {
        template: './src/jade/index-replace.pug',
        filename: 'index-replace.html'
    },
    {
        template: './src/jade/create.pug',
        filename: 'create.html'
    },
    {
        template: './src/jade/edit.pug',
        filename: 'edit.html'
    },
    {
        template: './src/jade/start.pug',
        filename: 'start.html'
    },
    {
        template: './src/jade/product.pug',
        filename: 'product.html'
    }
];

module.exports = {
    pages: pages
};